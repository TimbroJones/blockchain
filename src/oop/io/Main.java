package oop.io;

import java.util.Scanner;

import oop.blockchain.Block;
import oop.blockchain.Blockchain;
import oop.blockchain.Configuration;

public class Main implements Configuration{

	public static void main(String[] args){
		Main start = new Main();
		while(true)
			start.cmdAction();
	}

	private Scanner scan;
	private Block block;
	private Blockchain chain;
	
	public Main() {
		scan = new Scanner(System.in);
	}

	public String in() {
		System.out.print(CLI);
		return scan.nextLine();		
	}
	
	public void out(String output) {
		System.out.println(output);
	}
	
	
	public void cmdAction() {
		String input = in();
		String cmd = input.split(" ")[0];
		String arguments = input.replace(cmd,  "").trim();		
		
		if (cmd.equals("createBlockChain")) {
			createBlockChain();
		}
		else if (cmd.startsWith("createBlock")) {
			createBlock(arguments);
		}
		else if (cmd.startsWith("genHash")) {
			genHash(arguments);
		}
		else if (cmd.startsWith("createHash")) {
			createHash();
		}
		else if (cmd.startsWith("addBlock")) {
			addBlock();
		}
		else if (cmd.startsWith("validate")) {
			validate();
		}
		else if (cmd.startsWith("setHash")) {
			String[] params = arguments.split(" ");
			if(params.length < 2) {
				out("too few arguments");
			} else {
				try {
					int number = Integer.parseInt(params[1]);	
					setHash(params[0], number);
				} catch (NumberFormatException nfe) {
					out(params[1] +  " can't be converted into a Number");
				}
				
			}
		}
		else if(cmd.equals("q")) {
			out("Programm wurde beendet");
			System.exit(0);
		}
		else {
			out("ERROR! Wrong command");
		}
	}
		
	
	public void createBlockChain() {
		chain = new Blockchain();
		out("Blockchain created");
	}
	
	public void createBlock(String data) {
		if (chain == null || chain.blocks.size() == 0) {
			block = new Block(data);
			out("first Block created");
		}
		else {
			block = new Block(chain.blocks.get(chain.blocks.size() -1 ).getHash(), data);
			out("new Block created");
		}
	}
	
	public void createHash() {
		block.CreateBlock();
		out("Block created: " + block.getHash() + " nonce=" + block.getNonce());
	}
	
	public void addBlock() {
		chain.AddBlock(block);
		out("add Block to chain");
	}
	
	public void validate() {
		out("Blockchain is Valid: " + chain.validate());
	}
		
	public void setHash(String value, int number) {
		chain.getBlockByIndex(number).setHash(value);
		out("Hash set");
	}
	
	void genHash(String content) {
		out("generated hash: " + oop.blockchain.HashGenerator.GeneratHashFromString(content)); 
	}
	
}

