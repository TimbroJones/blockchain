package oop.blockchain;

import java.util.ArrayList;

public class Blockchain {
	
	public ArrayList<Block> blocks = new ArrayList<Block>();
	
	public void AddBlock(Block block) {
		blocks.add(block);
	}
	
	public void CreateBlock() {
	}
	
	public Block getBlockByIndex(int index) {
		return blocks.get(index);
	}
	
	public String validate() {
		 for(Block block: blocks) {
			if(block.getHash() == null) {
				return "false";
			}
		 	if(!block.getPreviousHash().equals(blocks.indexOf(block) == 0 ? "0" : blocks.get(blocks.indexOf(block)-1).getHash())) {
		 		return "Previous Hashes not equal";
		 	}
		 	if(!block.getHash().equals(HashGenerator.GeneratHashFromString(block.toString()))) {
		 		return "Current Hashes not equal";
		 	}
		 }
		 return "true";
	}
	
}
