package oop.blockchain;

public class HashGenerator {
	
	//This prime number is used to generate the hash
	final static long prime = 0xcbf29ce484222325L;
	
	public static String GeneratHashFromString(String content) {
		long hash = 0;
		
		if (content.length() < 2) content += "SoftwareprojektSS2019";
		
		for (int i = 0; i < content.length(); i++) {
			hash *= prime;
			hash ^= content.charAt(i);
		}
		
		return Long.toHexString(hash);
	}	
}
