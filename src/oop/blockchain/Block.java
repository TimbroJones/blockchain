package oop.blockchain;

public class Block implements Configuration{
	
	public Block(String data) {
		this("0", data);
	}
	
	public Block(String previousHash, String data) {
		if (previousHash == null) {
			previousHash = "0";
		}
		this.previousHash = previousHash; 
		this.data = data;
		timeStamp = System.currentTimeMillis();
		nonce = 0;
	}
	

	private String hashValue;
	public String getHash() {
		return hashValue;
	}
	public void setHash(String hash) {
		hashValue = hash;
	}

	private String previousHash;
	public String getPreviousHash() {
		return previousHash;
	}
	
	private long timeStamp;
	public long getTimeStamp() {
		return timeStamp;
	}

	private String data;
	public String getData() {
		return data;
	}

	private int nonce;
	public int getNonce() {
		return nonce;
	}
	
	public void CreateBlock() {
		do {
			setHash(HashGenerator.GeneratHashFromString(this.toString()));
			if (hashValue.startsWith(TARGET)) break;
			nonce++;
		} while(true);
	}
	
	public String toString() {
		return getPreviousHash() + Long.toString(getTimeStamp()) + Long.toString(getNonce()) + getData();
	}
	
}
